Sub CalcMetricsQuarter()
    Dim s As String: s = "data"
    Dim wks As Worksheet
    Set wks = ThisWorkbook.ActiveSheet
    Dim year As Integer
    Dim Quarter As Integer
    Dim Month1 As String
    Dim Month2 As String
    Dim Month3 As String
    Dim FirstMonth As Integer
    Dim LastMonth As Integer
    Dim MidMonth As Integer
    Dim FinalDate As Date
    Dim maxGM As Integer
    firstrow = 12
    LastRow = Range("A" & Rows.Count).End(xlUp).Row

'InputBoxes for Year and Quarter
year = InputBox("What Year is data needed for?")
Quarter = InputBox("What Quarter is data needed for? (Use 1, 2, 3, or 4)")

Application.ScreenUpdating = False

'Format Header
Call FormatHeader

'Define Months
If Quarter = 1 Then
    Month1 = "Jan"
    Month2 = "Feb"
    Month3 = "Mar"
    FirstMonth = 1
    MidMonth = 2
    LastMonth = 3
    FinalDate = "4/01/" & year
End If

If Quarter = 2 Then
    Month1 = "Apr"
    Month2 = "May"
    Month3 = "June"
    FirstMonth = 4
    MidMonth = 5
    LastMonth = 6
    FinalDate = "7/01/" & year
End If

If Quarter = 3 Then
    Month1 = "July"
    Month2 = "Aug"
    Month3 = "Sept"
    FirstMonth = 7
    MidMonth = 8
    LastMonth = 9
    FinalDate = "10/01/" & year
End If

If Quarter = 4 Then
    Month1 = "Oct"
    Month2 = "Nov"
    Month3 = "Dec"
    FirstMonth = 10
    MidMonth = 11
    LastMonth = 12
    FinalDate = "1/01/" & year + 1
End If

Range("A2") = year
Range("B2") = Month1
Range("C2") = Month2
Range("D2") = Month3

Cells(1, 12).Value = FirstMonth & "/01/" & year
Cells(2, 12).Value = MidMonth & "/01/" & year
Cells(3, 12).Value = LastMonth & "/01/" & year
Cells(4, 12).Value = FinalDate

'Find Max Table Count Size to be able to reference correct cells for GMs + Players
maxGM = Application.WorksheetFunction.Max(Columns("N"))

'Format Date to remove time
Call DateFormat

'Clear counts
Range("B3:F9").ClearContents
Range("B3:F3").Value = 0
Range("B5:F5").Value = 0
Range("B7:F7").Value = 0
Range("B9:F9").Value = 0

'First Month
i = firstrow
Do Until i > LastRow
    'Check if session occured during first month of quarter
    If Cells(i, 12).Value >= Cells(1, 12) And Cells(i, 12).Value < Cells(2, 12) Then
        If Cells(i, 6).Value = "Pathfinder Roleplaying Game (1st Edition)" Then Cells(3, 2).Value = Cells(3, 2).Value + Cells(i, 14).Value
        If Cells(i, 6).Value = "Pathfinder Roleplaying Game (2nd Edition)" Then Cells(5, 2).Value = Cells(5, 2).Value + Cells(i, 14).Value
        If Cells(i, 6).Value = "Starfinder Roleplaying Game" Then Cells(7, 2).Value = Cells(7, 2).Value + Cells(i, 14).Value
        If Cells(i, 6).Value = "Pathfinder Adventure Card Game" Then Cells(9, 2).Value = Cells(9, 2).Value + Cells(i, 14).Value
    End If
i = i + 1
Loop

'Second Month
i = firstrow
Do Until i > LastRow
    'Check if session occured during second month of quarter
    If Cells(i, 12).Value >= Cells(2, 12) And Cells(i, 12).Value < Cells(3, 12) Then
        If Cells(i, 6).Value = "Pathfinder Roleplaying Game (1st Edition)" Then Cells(3, 3).Value = Cells(3, 3).Value + Cells(i, 14).Value
        If Cells(i, 6).Value = "Pathfinder Roleplaying Game (2nd Edition)" Then Cells(5, 3).Value = Cells(5, 3).Value + Cells(i, 14).Value
        If Cells(i, 6).Value = "Starfinder Roleplaying Game" Then Cells(7, 3).Value = Cells(7, 3).Value + Cells(i, 14).Value
        If Cells(i, 6).Value = "Pathfinder Adventure Card Game" Then Cells(9, 3).Value = Cells(9, 3).Value + Cells(i, 14).Value
    End If
i = i + 1
Loop

'Third Month
i = firstrow
Do Until i > LastRow
    'Check if session occured during last month of quarter
    If Cells(i, 12).Value >= Cells(3, 12) And Cells(i, 12).Value < Cells(4, 12) Then
        If Cells(i, 6).Value = "Pathfinder Roleplaying Game (1st Edition)" Then Cells(3, 4).Value = Cells(3, 4).Value + Cells(i, 14).Value
        If Cells(i, 6).Value = "Pathfinder Roleplaying Game (2nd Edition)" Then Cells(5, 4).Value = Cells(5, 4).Value + Cells(i, 14).Value
        If Cells(i, 6).Value = "Starfinder Roleplaying Game" Then Cells(7, 4).Value = Cells(7, 4).Value + Cells(i, 14).Value
        If Cells(i, 6).Value = "Pathfinder Adventure Card Game" Then Cells(9, 4).Value = Cells(9, 4).Value + Cells(i, 14).Value
    End If
i = i + 1
Loop

'Toggle Month
ActiveSheet.Range("$A$11:$ZA$666666").AutoFilter Field:=12, Criteria1:=">=" & ActiveSheet.Range("L1"), _
    Operator:=xlAnd, _
    Criteria2:="<" & ActiveSheet.Range("L4")
    
'Hide PC Data
HidePCdata (maxGM)

'All System Unique GMs and Players
Cells(2, 5).Value = CountUniqueVisible(ActiveSheet.Range(Cells(firstrow, 20 + maxGM), Cells(LastRow, 20 + maxGM + 1200))) - 1
Cells(2, 6).Value = CountUniqueVisible(ActiveSheet.Range(Cells(firstrow, 20), Cells(LastRow, 20 + maxGM - 1))) - 1

'PF1 Unique GMs and Players
If Find_First("Pathfinder Roleplaying Game (1st Edition)") = True Then
    ToggleSystem ("Pathfinder Roleplaying Game (1st Edition)")
    Cells(3, 5).Value = CountUniqueVisible(ActiveSheet.Range(Cells(firstrow, 20 + maxGM), Cells(LastRow, 20 + maxGM + 1200))) - 1 'PF1 PC
    Cells(3, 6).Value = CountUniqueVisible(ActiveSheet.Range(Cells(firstrow, 20), Cells(LastRow, 20 + maxGM - 1))) - 1  'PF1 GM
    ActiveSheet.Range("$A$11:$ZA$666666").AutoFilter Field:=6
Else
    Cells(3, 5).Value = 0
    Cells(3, 6).Value = 0
End If

'PF2 Unique GMs and Players
If Find_First("Pathfinder Roleplaying Game (2nd Edition)") = True Then
    ToggleSystem ("Pathfinder Roleplaying Game (2nd Edition)")
    Cells(5, 5).Value = CountUniqueVisible(ActiveSheet.Range(Cells(firstrow, 20 + maxGM), Cells(LastRow, 20 + maxGM + 1200))) - 1 'PF2 PC
    Cells(5, 6).Value = CountUniqueVisible(ActiveSheet.Range(Cells(firstrow, 20), Cells(LastRow, 20 + maxGM - 1))) - 1 'PF2 GM
    ActiveSheet.Range("$A$11:$ZA$666666").AutoFilter Field:=6
Else
    Cells(5, 5).Value = 0
    Cells(5, 6).Value = 0
End If

'SF Unique GMs and Players
If Find_First("Starfinder Roleplaying Game") = True Then
    ToggleSystem ("Starfinder Roleplaying Game")
    Cells(7, 5).Value = CountUniqueVisible(ActiveSheet.Range(Cells(firstrow, 20 + maxGM), Cells(LastRow, 20 + maxGM + 1200))) - 1 'SF PC
    Cells(7, 6).Value = CountUniqueVisible(ActiveSheet.Range(Cells(firstrow, 20), Cells(LastRow, 20 + maxGM - 1))) - 1 'SF GM
    ActiveSheet.Range("$A$11:$ZA$666666").AutoFilter Field:=6
Else
    Cells(7, 5).Value = 0
    Cells(7, 6).Value = 0
End If

'ACG Unique GMs and Players
If Find_First("Pathfinder Adventure Card Game") = True Then
    ToggleSystem ("Pathfinder Adventure Card Game")
    Cells(9, 5).Value = CountUniqueVisible(ActiveSheet.Range(Cells(firstrow, 20 + maxGM), Cells(LastRow, 20 + maxGM + 1200))) - 1 'ACG PC
    Cells(9, 6).Value = CountUniqueVisible(ActiveSheet.Range(Cells(firstrow, 20), Cells(LastRow, 20 + maxGM - 1))) - 1 'ACG GM
    ActiveSheet.Range("$A$11:$ZA$666666").AutoFilter Field:=6
Else
    Cells(9, 5).Value = 0
    Cells(9, 6).Value = 0
End If

ActiveSheet.ShowAllData
'Range("L1:L4").ClearContent

End Sub

Function FormatHeader()
'Create empty rows and format if new sheet
If Range("A1") = "Name" Then
    Range("A1:A10").EntireRow.Insert
    Columns("A").ColumnWidth = 32
    Columns("E").ColumnWidth = 15
    Columns("F").ColumnWidth = 15
    Range("B1") = "Sessions/Quarter"
    Range("E1") = "Unique Players"
    Range("F1") = "Unique GMs"
    Range("A3") = "Pathfinder 1st edition"
    Range("A5") = "Pathfinder 2nd edition"
    Range("A7") = "Starfinder"
    Range("A9") = "ACG"
End If

End Function

Function DateFormat()

'warhorn date has T + time in it. This removes everything after the T so that only the date remains
For Each c In Range("L:L")
    If InStr(c.Value, "T") > 0 Then c.Value = Left(c.Value, InStr(c.Value, "T") - 1)
Next c

End Function

Function ToggleSystem(sys As String)

        ActiveSheet.Range("$A$11:$ZA$666666").AutoFilter Field:=6, Criteria1:= _
            sys
End Function

Function CountUniqueVisible(Target As Range)
''==============================================
''Return the # of unique items in visible cells in a selected range
''Created 29 July 2011 by Denis Wright
''source: https://www.mrexcel.com/board/threads/vba-count-of-distinct-values-in-filter-range.567887/post-2808034
''==============================================
    Dim Rng As Range, _
        c As Range
    Dim dic As Object
    Dim y
    Dim j As Long
    Dim Sht As Worksheet
    Dim strSheets As String
    
    Set dic = CreateObject("Scripting.Dictionary")
    Set Rng = Target.SpecialCells(xlCellTypeVisible)
    j = 0
    For Each c In Rng
        If Not dic.exists(c.Value) Then
            j = j + 1
            dic.Add c.Value, j
        End If
    Next c
    y = dic.keys
    'Now we have a list of unique values. Next step is to return the count.
    CountUniqueVisible = UBound(y) + 1

ExitHere:
    Set dic = Nothing
    Set Rng = Nothing
End Function

Function Find_First(y As String)
'Used to Find if a system is present in the data to know whether or not the auto filter can be toggled.
Dim FindString As String
Dim Rng As Range
FindString = y
If Trim(FindString) <> "" Then
    With ActiveSheet.Range("F:F")
        Set Rng = .Find(What:=FindString, _
                        After:=.Cells(.Cells.Count), _
                        lookIn:=xlValues, _
                        LookAt:=xlWhole, _
                        SearchOrder:=xlByRows, _
                        SearchDirection:=xlNext, _
                        MatchCase:=False)
        If Not Rng Is Nothing Then
            Find_First = True
        Else
            Find_Furst = False
        End If
    End With
End If
End Function

Function HidePCdata(y As Integer)

'prevents duplicate counting of unique players
'MaxGM is passed as y, and starting at 21st+y column, every other column will be hidden. these are the columns with PC data instead of player data.
For i = 21 + y To Range("ZA1").Column Step 2
    Columns(i).EntireColumn.Hidden = True
Next i

End Function
