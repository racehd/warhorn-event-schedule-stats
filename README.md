# Warhorn Event Schedule Stats

A VBA script that works with a Warhorn event schedule csv extract and parses some basic  stats about number of sessions run by system, unique GMs, and unique players per quarter. 

> I originally created this script while volunteering as a Venture Officer for Paizo's Organized Play Program. Although I have since retired from my volunteer role, I'm sharing this project as-is. Please note that I cannot guarantee updates, new features, bug fixes, or support.

## Instructions
This will walk through setting up the macro and executing it. 

### Set-up

1. In Warhorn, navigate to `Manage` -> `Event Schedule`. Then click the `Download CSV` button. 
2. Open the `.csv` file in MS Excel and save as a `.xlsm` which is a macro enabled workbook. 
3. In Excel, enable the [Developer Tab](https://support.microsoft.com/en-us/office/show-the-developer-tab-e1192344-5e56-4d45-931b-e5fd9bea2d45) if you do not have it already enabled. 
4. Go to the `Developer Tab` and click on `Visual Basic`.
5. Navigate to `File->Import` and import [vba-warhorn-stats.bas](./vba-warhorn-stats.bas). 
6. Exit the Visual Basic window.
7. Close the sheet completely and re-open it. Excel will ask you if you want to enable/trust the macros. Agree to enable macros for this workbook. 

### Usage
1. Navigate to the `Developer Tab` and click on `Macros`. A macro box will pop-up. Click on `CalcMetricsQuarter` and then click `Run`. 
2. Two pop-ups will occur one after another asking for the year and which quarter (1-4) you are interested in. Type your response and click `OK` for each. 
3. The macro will run through your data for the selected quarter. For performance it will have the excel screen updating turned off so wait until you see the screen update with your quarterly stats broken out by system. 
4. For future uses, you can bring ini the new report as a new sheet, select the sheet, and then run the macro. It will run on the actively selected sheet. 

## Known Limitations
- Occasionally, the script will incorrectly count metrics. Run the script a second time if the numbers seem off. 
- The macro counts waitlisted players as part of the unique players. The rationale is that these are players who were interested in playing games in your event regardless of whether or not you were able to meet their demand.
- Since the macro is relying on Warhorn event data instead of the Paizo event data, this means it does not capture which players actually showed up to each session unless you reconciled Warhorn data after the session. 

## Licensing
**Project Licensing**
- All VBA in this project is licensed under the MIT License. 

**Content Usage and Licensing**
- This project is NOT licensed by, endorsed by, affiliated with, or otherwise associated with Warhorn nor Paizo. 